$(document).ready(
function ( )
{
			
/**
 *  Initialize viewmodel
 */
var validering = new RCC.Validation();
var vm = new RCC.ViewModel( { validation: validering } );
window.vm = vm;

// st�ng av validering under initializering av formul�r
vm.$validation.enabled(false);

// varningsmeddelanden
var C_WARNING_INGEN_ANMALAN = "Anm�lan i MDS-registret saknas f�r denna patient.\n\nF�r att kunna registrera en uppf�ljning s� m�ste det f�rst finnas en anm�lan registrerad.\n\nDu kan fylla i formul�ret och spara i din inkorg genom alternativet 'Ej klar, kvar i inkorg' i �tg�rdslistan f�r att sedan �teruppta �rendet och spara i registret n�r en anm�lan finns registrerad.";
var C_WARNING_ANMALAN_FINNS = "Anm�lan i MDS-registret finns registrerad f�r denna patient.\n\nFyll i formul�ret och v�lj 'Spara i register' i �tg�rdslistan f�r att slutf�ra registreringen alternativt 'Ej klar, kvar i inkorg' f�r att delspara i din inkorg och forts�tta registreringen vid ett senare tillf�lle.";
var C_WARNING_FLERA_ANMALAN = "Denna patient har mer �n en anm�lan registrerad i MDS-registret.\n\nV�lj den anm�lan uppf�ljningen g�ller i rutan 'Information'.";
var C_WARNING_UPPFOLJNING_FINNS = "Uppf�ljning i MDS-registret finns registrerad tidigare.\n\nUppgifter fr�n den registrerade uppf�ljningen h�mtas automatiskt till formul�ret. Fyll i aktuella uppgifter och v�lj �nskad �tg�rd.\n\nObservera att befintliga uppgifter i den registrerade uppf�ljningen kommer att skrivas �ver vid �tg�rden 'Spara i register'.";
var C_WARNING_EJ_UPPFOLJD = "Om patienten ej f�ljts upp vid er klinik var god skicka tillbaka �rendet genom alternativet 'Kan ej fyllas i, skicka tillbaka' i �tg�rdslistan.";
var C_WARNING_EJ_UPPFOLJD_KRYSS = "Var god ange att patienten ej kan f�ljas upp med kryssrutan nedan och ange �ven uppf�ljande klinik/sjukhus om det �r k�nt innan �rendet skickas tillbaka.";

var requestCount = ko.observable(0);

// inrapporterande anv�ndare
vm.reportingUser = ko.observable(false);

// p�minnelse status
vm.reminderStatus = ko.observable(false);

// �ters�nt status
vm.resentStatus = ko.observable(false);

// form initialized
vm.initialized = ko.observable(false);

// kontrollera om det g�ller en inrapportering
if(!inca.form.isReadOnly && inca.errand.status.val() != null){
	// s�tt inrapporterande variabel till sann
	vm.reportingUser(true);
}

// kontrollera om det �rendet har status p�minnelse
if(inca.errand.status.val() == 'P�minnelse'){
	// s�tt inrapporterande variabel till sann
	vm.reminderStatus(true);
}

// kontrollera om det �rendet har status p�minnelse
if(inca.errand.status.val() == '�ters�nt'){
	// s�tt inrapporterande variabel till sann
	vm.resentStatus(true);
}


/**
 * Submit callback
 */
inca.errand.submitValidationCallback = function () {
	var actionsWithoutValidation = ['1','57'];

	// om tomt val.
	if(inca.errand.action.val() == ''){
		return false;
	}
	
	// om avbryt och radera. 
	if(inca.errand.action.val() == 6)
	{
		if(!warningDialog(C_WARNING_AVBRYT))
			return false;
		else 
			return true;
	}
	
	// om makulera 
	if(inca.errand.action.val() == 46)
	{
		if(!warningDialog(C_WARNING_MAKULERA))
			return false;
		else 
			return true;
	}
	
	// om Kan ej fyllas i, skicka tillbaka 
	if(inca.errand.action.val() == 57 && !vm.U_ejuppfoljd() && !vm.editingRegisterPost()) 
	{
		alert(C_WARNING_EJ_UPPFOLJD_KRYSS);
		return false;
	}
	
	// om �tg�rd utan validering
	if ( $.inArray (inca.errand.action.val(),actionsWithoutValidation) >= 0) {
		return true;
	};
	
	// om inrapport�r f�rs�ker spara en ej uppf�ljd uppf�ljning i register
	if (vm.U_ejuppfoljd() && !inca.user.role.isReviewer) {
		alert(C_WARNING_EJ_UPPFOLJD);
		return false;
	};
	
	// om ingen anm�lan finns, hindra spara i register
	if (!vm.$vd.mds_anmalan_vd()) {
		// Visa alla fel och valideringsfel
		if(vm.$vd.mds_anmalan_vd.listValues().length == 0)
			alert(C_WARNING_INGEN_ANMALAN);
		else if(vm.$vd.mds_anmalan_vd.listValues().length > 1)
			alert(C_WARNING_FLERA_ANMALAN);
       return false;
	}
	
	// om ingen behandling �r vald, hindra spara i register 
	if(!vm.U_ejuppfoljd() && !vm.behandlingChecked())
	{
		alert('Behandling:\nInget val �r gjort');
		// s�tt focus p� felet
		$("#behandlingError").focus();
		return false;
	}
	
	// om inget benm�rgsprov �r valt, hindra spara i register
	if(!vm.U_ejuppfoljd() && vm.U_amltransform() && vm.U_amltransform().value == '1' && !vm.benmargsprovChecked())
	{
		alert('Benm�rgsprov som bekr�ftar transformationen:\nInget val �r gjort');
		// s�tt focus p� felet
		$("#benmargsprovError").focus();
		return false;
	}
	
	// aktivera formul�rvalidering
	vm.$validation.enabled(true);
	
	// samla fel
	var errors = validering.errors();
	
	// fel finns, visa och avbryt �tg�rd
	if (errors.length > 0) {
		// Visa alla fel och valideringsfel
		validering.markAllAsAccessed();
		
		// start bygget av felmeddelande och l�gg till antal fel
		var sErrorMessage = 'Formul�ret inneh�ller ' + errors.length + ' fel.\n';
		
		// l�gg till samtliga felande registervariablers beskrivningar och felmeddelanden
		ko.utils.arrayForEach(errors, function(error) {
        	sErrorMessage = sErrorMessage + '\n' + '- ' + vm[error.info.source.name].rcc.regvar.description +
        					 ': ' + '\n' + error.message;
    	});
    	
    	// visa felmeddelande
		alert(sErrorMessage);
		
		// s�tt focus p� f�rsta valideringsfelet
		$(".rcc-invalid:first").focus();
		
		// deaktivera formul�rvalidering
		vm.$validation.enabled(false);
			
       return false;
	}
	// inga fel, skicka 
	else {
       vm.actionsToDoBeforePost();
		return true;
	}
};



/**
 *  LOKALA FUNKTIONER
 */
 
 
/* LOADFUNKTIONER */
 
 
/**
 * Funktion f�r f�rberedande �tg�rder innan formul�rets initializeras
*
* @method runFunctionsOnLoad
*/
vm.runFunctionsOnLoad = function() {
	// (endast vid inrapportering)		
	if(vm.reportingUser()){
		// ladda v�rdedom�ner
		vm.loadVD();
		
		// initiera subscriber f�r uppf�ljningsv�rdedom�n
		vm.$vd.mds_uppfoljning_vd.subscribe(function (newValue) {
		    // Om en uppf�ljning �r vald, ladda data och fyll vm
		    if (newValue) {
		    	requestCount( requestCount() + 1 );
		        inca.form.getRegisterRecordData({
		            vdlist: "VD_mds_uppfoljning_vd",
		            value: newValue,
		            success: function (data) {
		            	if(vm.getSelectedUppfoljning(vm.$vd.mds_uppfoljning_vd()).U_beddat != null)
		            		RCC.ViewModel.fill({target: vm, data: data});
		            	requestCount( requestCount() - 1 );
		            },
		            error: function ( err )
						{
							requestCount( requestCount() - 1 );
							$('<div title="Ett fel har uppst�tt"></div>')
									.text( 'Lyckades inte ladda registrerade substanser: ' + err + ', ' + JSON.stringify(err) + '. F�rs�k att ladda om formul�ret.')
									.dialog( { buttons: { OK: function ( ) { $(this).close(); } } } );
							$(inca.form.getContainer()).hide();
						}
		        });
		    }
		    // annars, rensa vm
			else {
				vm.resetForm();
			}
		});
		
		
		// ange custom validering
		vm.setCustomValidation();
		
		// kopiera systemvariabler
		vm.copySystemVariables();
		
	}
	

	// modifiera listor
	vm.modifyLists();	

	// initiera subscribers f�r registervariabler
	vm.initializeSubscribers();
}


/**
 * Funktion f�r �tg�rder efter att formul�ret har initializeras
*
* @method runFunctionsAfterLoad
*/
vm.runFunctionsAfterLoad = function() {
	
	// varna anv�ndaren om det ej finns n�gon anm�lan att knyta uppf�ljningen till
	if (vm.$vd.mds_anmalan_vd.listValues().length == 0) {
		alert(C_WARNING_INGEN_ANMALAN);
	}
	// varna anv�ndaren att det redan finns en uppf�ljning registrerad men att denna kan uppdateras
	else if (vm.$vd.mds_anmalan_vd.listValues().length == 1 && (vm.$vd.mds_uppfoljning_vd.listValues() && vm.$vd.mds_uppfoljning_vd.listValues().length == 1) && vm.getSelectedUppfoljning(vm.$vd.mds_uppfoljning_vd()).U_beddat != null) {
		alert(C_WARNING_UPPFOLJNING_FINNS);
	}
	else if (vm.$vd.mds_anmalan_vd.listValues().length == 1) {
		//alert(C_WARNING_ANMALAN_FINNS);
	}
	// varna anv�ndaren om det finns flera anm�lningar att knyta uppf�ljningen till
	else if (vm.$vd.mds_anmalan_vd.listValues().length > 1) {
		alert(C_WARNING_FLERA_ANMALAN);
	}

	// initiera subscriber f�r anm�lningsrullistan
	vm.$vd.mds_anmalan_vd.subscribe(function (newValue) {
		// nollst�ll v�rde f�r uppf�ljningsv�rdedom�n
		vm.$vd.mds_uppfoljning_vd(undefined);
		
		// Om en anm�lan �r vald, ladda om v�rdedom�n f�r uppf�ljningar
		if(vm.$vd.mds_anmalan_vd()){
			vm.loadUppfoljningVD();
		}
	});

	// mutera registervariabler som kan p�verka validering
	vm.mutateValues();
	
	// monitor
	if (inca.user.role.isReviewer)
	{
		// �ters�nt �rende
		if (vm.resentStatus())
		{	
			if (orgFinns())
			{
				// kopiera reciver till inrapporterande sjukhus vid �ndring
				inca.errand.receiver.change(function()
				{
				var i = orgVal(); 
					if(i>0)
					{
						var org = inca.errand.receiver.selectedText();
						vm.U_annsjhklin(org);
						vm.U_annsjhkod(getHospitalCode(org));
						vm.U_annklinkod(getClinicCode(org));
					}
				});
			}
		}
	}
	
	// aktivera formul�rvalidering efter formul�ret �r initializerat
	// vm.$validation.enabled(true);
	
	// flagga formul�ret som f�rdig initilaizerat
	vm.initialized(true);
}


/**
 * Funktion f�r �tg�rder efter att formul�ret har initializeras
*
* @method runFunctionsAfterLoad
*/
vm.actionsToDoBeforePost = function() {
	// s�tt inrapporteringsdatum
	vm.U_rappdat(getDagensDatum());
}


/**
 * Funktion f�r att ladda v�rdedom�nslogik
*
* @method loadVD
*/
vm.loadVD = function() {
	// Initiera v�rdedom�nsvariabler
	vm.$vd.mds_anmalan_vd.listValues = ko.observableArray(undefined);
	vm.$vd.mds_uppfoljning_vd.listValues = ko.observableArray(undefined);
	
	// Ladda v�rdedom�n f�r anm�lan
	vm.loadAnmalanVD();

	// Om en anm�lan redan �r sparad, ladda v�rdedom�n f�r uppf�ljningar
	if(vm.$vd.mds_anmalan_vd()){
		vm.loadUppfoljningVD();
	}
}
			

/**
 * Funktion f�r att ladda anm�lningsv�rdedom�n
*
* @method loadAnmalanVD
*/
vm.loadAnmalanVD = function() {
	requestCount( requestCount() + 1 );
	inca.form.getValueDomainValues(
		{
			vdlist: "VD_mds_anmalan_vd",
			parameters: { patientid: inca.form.env._PATIENTID },
			success:
				function ( list )
				{

					// Fyll v�rdedom�nvariabelns lista med registrerade anm�lningar
					vm.$vd.mds_anmalan_vd.listValues( list );
					// Om det endast finns en anm�lning registrerad s�tt v�rdedom�nvariabeln till denna
					if(!vm.$vd.mds_anmalan_vd() && vm.$vd.mds_anmalan_vd.listValues().length == 1){
						vm.$vd.mds_anmalan_vd(vm.$vd.mds_anmalan_vd.listValues()[0].id);
						vm.loadUppfoljningVD();
					}
					requestCount( requestCount() - 1 );
				},
			error:
				function ( err )
				{
					requestCount( requestCount() - 1 );
					$('<div title="Ett fel har uppst�tt"></div>')
							.text( 'Lyckades inte ladda registrerade substanser: ' + err + ', ' + JSON.stringify(err) + '. F�rs�k att ladda om formul�ret.')
							.dialog( { buttons: { OK: function ( ) { $(this).close(); } } } );
					$(inca.form.getContainer()).hide();
				}
		} );
}
			

/**
 * Funktion f�r att ladda uppf�ljningsv�rdedom�n
*
* @method loadUppfoljningVD
*/
vm.loadUppfoljningVD = function() {
	requestCount( requestCount() + 1 );
	
	inca.form.getValueDomainValues(
	{
		vdlist: "VD_mds_uppfoljning_vd",
		parameters: { patientid: inca.form.env._PATIENTID, key: vm.$vd.mds_anmalan_vd() },
		success:
			function ( list )
			{
				// Fyll v�rdedom�nvariabelns lista med registrerade uppf�ljningar
				vm.$vd.mds_uppfoljning_vd.listValues( list );
				// Om det endast finns en uppf�ljning registrerad s�tt v�rdedom�nvariabeln till denna
				if(!vm.$vd.mds_uppfoljning_vd() && vm.$vd.mds_uppfoljning_vd.listValues().length == 1){
					vm.$vd.mds_uppfoljning_vd(vm.$vd.mds_uppfoljning_vd.listValues()[vm.$vd.mds_uppfoljning_vd.listValues().length - 1].id);
				}
				requestCount( requestCount() - 1 );
			},
		error:
			function ( err )
			{
				requestCount( requestCount() - 1 );
				$('<div title="Ett fel har uppst�tt"></div>')
						.text( 'Lyckades inte ladda registrerade substanser: ' + err + ', ' + JSON.stringify(err) + '. F�rs�k att ladda om formul�ret.')
						.dialog( { buttons: { OK: function ( ) { $(this).close(); } } } );
				$(inca.form.getContainer()).hide();
			}
	} );	
}


/**
* Funktion f�r att returnera en registerpost fr�n MDS Anm�lan v�rdedom�n som ett dataobjekt.
*
* @method getSelectedAnmalan
* @param {String} pk Prim�rnyckelv�rde f�r dataobjekt 
* @return {Object} Dataobject 
*/
vm.getSelectedAnmalan = function(pk) {
	if (pk != null) {
		var item = ko.utils.arrayFirst(vm.$vd.mds_anmalan_vd.listValues() || [], function (item) {
			return item.id == pk;
		});
		
		if(!item) 
			return undefined;
			
		return item.data;
	}
	else {
		return undefined;
	}
};
			

/**
* Funktion f�r att returnera en registerpost fr�n MDS Uppf�ljning v�rdedom�n som ett dataobjekt.
*
* @method getSelectedUppfoljning
* @param {String} pk Prim�rnyckelv�rde f�r dataobjekt 
* @return {Object} Dataobject 
*/
vm.getSelectedUppfoljning = function(pk) {
	if (pk != null) {
		var item = ko.utils.arrayFirst(vm.$vd.mds_uppfoljning_vd.listValues() || [], function (item) {
			return item.id == pk;
		});
		
		if(!item) 
			return undefined;
			
		return item.data;
	}
	else {
		return undefined;
	}
};


/**
* Funktion f�r att �terst�lla samtliga registervariabler i ett formul�r
* Systemvariabler kopieras p� nytt n�r formul�ret �r �terst�llt
*
* @method resetForm 
*/
vm.resetForm = function() {
	// rensa alla variabler i formul�ret
	for (var obj in vm)
	{
		if (obj.substring(0,1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc){
			vm[obj](undefined);
		}
	}
	
	// �terst�ll systemvariabler
	vm.copySystemVariables();
};


/**
* Funktion f�r att kopiera utvalda systemvariabler till registrets variabler(om inte redan sparade)
*
* @method copySystemVariables
*/
vm.copySystemVariables = function() {
	var reporterName = null;
	var inUnitName = null;
	
	if(vm.reminderStatus()){
		reporterName = inca.user.firstName + " " + inca.user.lastName;
		inUnitName = inca.user.position.fullNameWithCode;
	}
	else{
		reporterName = vm.$env._REPORTERNAME;
		inUnitName = vm.$env._INUNITNAME;
	}
	
	// initierad av
	if(!vm.U_initav())
		vm.U_initav(vm.$env._INREPNAME);
	
	// inrapport�r
	vm.U_inrapp(reporterName);
	
	// inrapporterande sjukhus
	vm.U_sjuklin(inUnitName);
	if(vm.U_sjuklin()){
		// sjukhus kod
		vm.U_sjukhus(getHospitalCode(inUnitName));
		// klinik kod
		vm.U_klinik(getClinicCode(inUnitName));
	}
}


/**
* Funktion f�r att ange formul�rspecifik validering f�r valda registervariabler
*
* @method setCustomValidation
*/
vm.setCustomValidation = function() {
	// ange ej obligatoriska variabler
	vm.U_annsjhklin.rcc.validation.required(false);
	vm.U_annsjhkod.rcc.validation.required(false);
	vm.U_annklinkod.rcc.validation.required(false);
	vm.U_kommentar.rcc.validation.required(false);
	vm.U_rappdat.rcc.validation.required(false);
	vm.U_mcv.rcc.validation.required(false);
	vm.U_neugran.rcc.validation.required(false);
	vm.U_antmon.rcc.validation.required(false);
	vm.U_blastblod.rcc.validation.required(false);
	vm.U_procentneutro.rcc.validation.required(false);
	vm.U_procentantmon.rcc.validation.required(false);
	vm.U_procentblast.rcc.validation.required(false);
	vm.U_ld.rcc.validation.required(false);
	vm.U_seryt.rcc.validation.required(false);
	vm.U_sferr.rcc.validation.required(false);
	vm.U_karyofulltxt.rcc.validation.required(false);
	vm.U_amlbiobank.rcc.validation.required(false);
	vm.U_amlbiobankdat.rcc.validation.required(false);
	vm.U_pathladat.rcc.validation.required(false);
	vm.U_remsjhklin.rcc.validation.required(false);
	vm.U_remsjhkod.rcc.validation.required(false);
	vm.U_remklinkod.rcc.validation.required(false);
	vm.$vd.mds_uppfoljning_vd.rcc.validation.required(false);
	
	// LPK validering, min max v�rden
	//resetValidation(vm.U_lpk);
	//vm.U_lpk.rcc.validation.add( new RCC.Validation.Tests.Decimal( vm.U_lpk, {decimalPlaces: 1, min: 0, max: 200, ignoreMissingValues: true} ) );
	//vm.U_lpk.rcc.validation.add( new RCC.Validation.Tests.NotMissing(vm.U_lpk) );
	
	// HB validering, min max v�rden
	resetValidation(vm.U_hb);
	vm.U_hb.rcc.validation.add( new RCC.Validation.Tests.Integer( vm.U_hb, {min: 0, max: 200, ignoreMissingValues: true} ) );
	vm.U_hb.rcc.validation.add( new RCC.Validation.Tests.NotMissing(vm.U_hb) );
	
	// Procent validering, min max v�rden
	resetValidation(vm.U_procentneutro);
	vm.U_procentneutro.rcc.validation.add( new RCC.Validation.Tests.Integer( vm.U_procentneutro, { min: 0, max: 100, ignoreMissingValues: true } ) );
	resetValidation(vm.U_procentantmon);
	vm.U_procentantmon.rcc.validation.add( new RCC.Validation.Tests.Integer( vm.U_procentantmon, { min: 0, max: 100, ignoreMissingValues: true } ) );
	resetValidation(vm.U_procentblast);
	vm.U_procentblast.rcc.validation.add( new RCC.Validation.Tests.Integer( vm.U_procentblast, { min: 0, max: 100, ignoreMissingValues: true } ) );
}


/**
* Funktion f�r att manipulera registerspcifika listor
*
* @method modifyLists
*/
vm.modifyLists = function() {
	// to bort inledande tom rad fr�n listor
	sliceFirstInList(vm.U_cellhalt);
	sliceFirstInList(vm.U_jakv);
	sliceFirstInList(vm.U_benprovutf);
	sliceFirstInList(vm.U_ringblast);
	sliceFirstInList(vm.U_fibros23);
	sliceFirstInList(vm.U_annbm);
	sliceFirstInList(vm.U_cytoutf);
	sliceFirstInList(vm.U_karyotyp);
	sliceFirstInList(vm.U_fishutf);
	sliceFirstInList(vm.U_erotran);
	sliceFirstInList(vm.U_behenl);
	sliceFirstInList(vm.U_amltransform);
	sliceFirstInList(vm.U_amlbiobank);
	sliceFirstInList(vm.U_stamtransutf);
	sliceFirstInList(vm.U_stamtransplan);
	sliceFirstInList(vm.U_pathla);
	sliceFirstInList(vm.U_patavliden);
	sliceFirstInList(vm.U_patavlidenmds);
	
	sliceList(vm.U_behenl,0,2);
	
	// Ers�tt spcialtecken i listor	
	replaceInList(vm.U_karyotyp, "= eller >", "\u2265");
}


/**
* Funktion f�r att rensa v�rden vars logik �r knutna till registervariablen U_lpk
*
* @method clearLPKDependentValues
*/
vm.clearLPKDependentValues = function() {	
 	// rensa v�rden
 	vm.U_neugran(undefined);
 	vm.U_procentneutro(undefined);
 	vm.U_antmon(undefined);
 	vm.U_procentantmon(undefined);
 	vm.U_blastblod(undefined);
 	vm.U_procentblast(undefined);
 	
 	return true;
} 


/**
* Funktion f�r att ber�kna procentv�rde f�r Neutrofila granulocyter baserat p� LPK och Neutrofila granulocyter
*
* @method calculateProcentNeutro
*/
vm.calculateProcentNeutro = function() {	
	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_neugran.rcc.validation.errors().length == 0) {
	 	vm.U_procentneutro(getCalcualtedPercentValue(vm.U_lpk(),vm.U_neugran()));
	}
	return true;
}


/**
* Funktion f�r att ber�kna Neutrofila granulocyter baserat p� LPK och procentv�rde f�r Neutrofila granulocyter
*
* @method calculateNeuGran
*/
vm.calculateNeuGran = function() {	
 	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_procentneutro.rcc.validation.errors().length == 0) {
	 	vm.U_neugran(getCalcualtedDecimalValue(vm.U_lpk(),vm.U_procentneutro()));
	}
	return true;
}


/**
* Funktion f�r att ber�kna procentv�rde f�r Antal monocyter baserat p� LPK och Antal monocyter
*
* @method calculateProcentAntMon
*/
vm.calculateProcentAntMon = function() {	
	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_antmon.rcc.validation.errors().length == 0) {
	 	vm.U_procentantmon(getCalcualtedPercentValue(vm.U_lpk(),vm.U_antmon()));
	}
	return true;
}


/**
* Funktion f�r att ber�kna Antal monocyter baserat p� LPK och procentv�rde f�r Antal monocyter
*
* @method calculateAntMon
*/
vm.calculateAntMon = function() {	
 	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_procentantmon.rcc.validation.errors().length == 0) {
 		vm.U_antmon(getCalcualtedDecimalValue(vm.U_lpk(),vm.U_procentantmon()));
 	}
}


/**
* Funktion f�r att ber�kna procentv�rde f�r Antal blaster i blod baserat p� LPK och Antal blaster i blod
*
* @method calculateProcentBlast
*/
vm.calculateProcentBlast = function() {	
	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_blastblod.rcc.validation.errors().length == 0) {
		vm.U_procentblast(getCalcualtedPercentValue(vm.U_lpk(),vm.U_blastblod()));
	}
	return true;
}


/**
* Funktion f�r att ber�kna Antal blaster i blod baserat p� LPK och procentv�rde f�r Antal blaster i blod
*
* @method calculateBlastBlod
*/
vm.calculateBlastBlod = function() {	
 	if (vm.U_lpk() && vm.U_lpk.rcc.validation.errors().length == 0 && vm.U_procentblast.rcc.validation.errors().length == 0) {
		vm.U_blastblod(getCalcualtedDecimalValue(vm.U_lpk(),vm.U_procentblast()));
	}
	return true;
}


/**
* Funktion f�r att kontrollera v�rde f�r blaster i benm�rg
* Om v�rdet �verstiger 20.0 visas en varningsdialog
*
* @method checkBenBlastValue
*/
vm.checkBenBlastValue = function() {
	var value = vm.U_blastben();
		if(typeof value == 'string')
			value = value.replace(',','.');
		
	if(vm.U_blastben() && (parseFloat(value) > 20.0))
		alert("Om antalet blaster i benm�rgen �verstiger 20 % m�ste orsak anges till varf�r patienten inte anm�ls i AML-registret ist�llet");
}


/**
* Funktion f�r att initiera subscribers f�r spcifika registervariabler
*
* @method initializeSubscribers
*/
vm.initializeSubscribers = function() {
	 
	/* SUBSCRIBERS F�R "Patient �r ej uppf�ljd vid v�r klinik" */
	
	/**
	 * Subscriber f�r U_ejuppfoljd
	 */
	vm.U_ejuppfoljd.subscribe(function (newValue) {		
		
		if (newValue) {
			if(inca.user.role.isReviewer){
				enableValidation(vm.U_annsjhkod);	
				enableValidation(vm.U_annklinkod);
				enableValidation(vm.U_annsjhklin);	
			}				
			disableValidation(vm.U_inrapp);
			disableValidation(vm.U_sjuklin);
			disableValidation(vm.U_sjukhus);
			disableValidation(vm.U_klinik);
			vm.U_kommentar(undefined);
			disableValidation(vm.U_lakare);
			disableValidation(vm.U_beddat);
			disableValidation(vm.U_blodprovdat);
			disable(vm.U_natbioanm);
			disable(vm.U_natbioeftanm);
			disable(vm.U_natbioejtillf);
			disable(vm.U_natbioavboj);
			disableValidation(vm.U_benprovutf);
			disableValidation(vm.U_cytoutf);
			disableValidation(vm.U_erotran);
			disableValidation(vm.U_trotran);
			disable(vm.U_behind);
			disable(vm.U_behaza);
			disable(vm.U_behhyd);
			disable(vm.U_behery);
			disable(vm.U_behimm);
			disable(vm.U_behlen);
			disable(vm.U_behkel);
			disable(vm.U_behend);
			disable(vm.U_behann);
			disable(vm.U_behing);
			disableValidation(vm.U_amltransform);
			disableValidation(vm.U_stamtransutf);
			disableValidation(vm.U_patavliden);
			vm.U_remsjhklin(undefined);
			vm.U_remsjhkod(undefined);
			vm.U_remklinkod(undefined);
		}
		else{
			if(inca.user.role.isReviewer){
				disableValidation(vm.U_annsjhklin);
				disableValidation(vm.U_annsjhkod);	
				disableValidation(vm.U_annklinkod);	
			}

			enableValidation(vm.U_inrapp);
			enableValidation(vm.U_sjuklin);
			enableValidation(vm.U_sjukhus);
			enableValidation(vm.U_klinik);
			enableValidation(vm.U_lakare);
			enableValidation(vm.U_beddat);
			enableValidation(vm.U_blodprovdat);
			enableValidation(vm.U_benprovutf);
			enableValidation(vm.U_cytoutf);
			enableValidation(vm.U_erotran);
			enableValidation(vm.U_trotran);
			enableValidation(vm.U_amltransform);
			enableValidation(vm.U_stamtransutf);
			enableValidation(vm.U_patavliden);
			
			vm.U_annsjhklin(undefined);
			vm.U_annsjhkod(undefined);
			vm.U_annklinkod(undefined);
			
			vm.copySystemVariables();
		}
	});
	

	/* SUBSCRIBERS F�R "Blodprov" */
	
	/**
	 * Subscriber f�r U_blodprovdat
	 */
	vm.U_blodprovdat.subscribe(function (newValue) {		
		if (newValue) {
			enableValidation(vm.U_hb);
			//enableValidation(vm.U_mcv);
			enableValidation(vm.U_lpk);
			//enableValidation(vm.U_neugran);
			enableValidation(vm.U_tpk);
			//enableValidation(vm.U_ld);
			enableValidation(vm.U_jakv);
		}
		else{
			disableValidation(vm.U_hb);	
			//disableValidation(vm.U_mcv);
			vm.U_mcv(undefined);
			disableValidation(vm.U_lpk);
			//disableValidation(vm.U_neugran);
			vm.U_neugran(undefined);
			vm.U_antmon(undefined);
			vm.U_blastblod(undefined);
			disableValidation(vm.U_tpk);
			//disableValidation(vm.U_ld);
			vm.U_ld(undefined);
			disableValidation(vm.U_jakv);
			
			vm.U_procentneutro(undefined);
			vm.U_procentantmon(undefined);
			vm.U_procentblast(undefined);
			vm.U_seryt(undefined);
			vm.U_sferr(undefined);
		}
	});
	 
	 
	/* SUBSCRIBERS F�R "Prov till Nationella MDS biobanken" */
	
	/**
	 * Subscriber f�r U_natbioeftanm
	 */
	vm.U_natbioeftanm.subscribe(function (newValue) {		
		if (newValue) {
			enableValidation(vm.U_natbioeftanmdat);	
		}
		else{
			disableValidation(vm.U_natbioeftanmdat);
		}
	});
	 

	/* SUBSCRIBERS F�R "Benm�rgsstatus" */

	/**
	 * Subscriber f�r U_benprovutf
	 */
	vm.U_benprovutf.subscribe(function (newValue) {
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_benprovdat);
			disableValidation(vm.U_cellhalt);
			disable(vm.U_cellhaltsak);
			disable(vm.U_blastbenejbedom);
			disableValidation(vm.U_blastben);
			disableValidation(vm.U_ringblast);
			disableValidation(vm.U_fibros23);
			disableValidation(vm.U_annbm);
		}
		else{
			enableValidation(vm.U_benprovdat);
			enableValidation(vm.U_cellhalt);
			enableValidation(vm.U_blastben);
			enableValidation(vm.U_ringblast);
			enableValidation(vm.U_fibros23);
			enableValidation(vm.U_annbm);
		}
	});
	
	/**
	 * Subscriber f�r U_cellhaltsak
	 */
	vm.U_cellhaltsak.subscribe(function (newValue) {		
		if (vm.U_benprovutf() && vm.U_benprovutf().value == '1'){
			if (newValue) {
				disableValidation(vm.U_cellhalt);
			}
			else{
				enableValidation(vm.U_cellhalt);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_blastbenejbedom
	 */
	vm.U_blastbenejbedom.subscribe(function (newValue) {		
		if (vm.U_benprovutf() && vm.U_benprovutf().value == '1'){
			if (newValue) {
				disableValidation(vm.U_blastben);
			}
			else{
				enableValidation(vm.U_blastben);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_blastben
	 */
	vm.U_blastben.subscribe(function (newValue) {
		var value = newValue;
		if(typeof value == 'string')
			value = value.replace(',','.');
		
		if(vm.U_blastben() && (parseFloat(value) > 20.0)){
			enableValidation(vm.U_blastbentxt);
		}
		else{
			disableValidation(vm.U_blastbentxt);
		}
		
	});

	/**
	 * Subscriber f�r U_annbm
	 */
	vm.U_annbm.subscribe(function (newValue) {
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_annbmtxt);
		}
		else{
			enableValidation(vm.U_annbmtxt);
		}
	});
	
	
	/* SUBSCRIBERS F�R HAR NY CYTOGENETIK UTF�RTS SEDAN DIAGNOS */

	/**
	 * Subscriber f�r U_cytoutf
	 */
	vm.U_cytoutf.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_karyotyp);
			disableValidation(vm.U_fishutf);
		}
		else{
			enableValidation(vm.U_karyotyp);
			enableValidation(vm.U_fishutf);
		}
	});
	
	/**
	 * Subscriber f�r U_karyotyp
	 */
	vm.U_karyotyp.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '8'){
			disableValidation(vm.U_karyoanntxt);
		}
		else{
			enableValidation(vm.U_karyoanntxt);
		}
	});
	
	/**
	 * Subscriber f�r U_fishutf
	 */
	vm.U_fishutf.subscribe(function (newValue) {
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_fishres);
		}
		else{
			enableValidation(vm.U_fishres);
		}
	});
	 
	 
	/* SUBSCRIBERS F�R "Behandling" */
	
	/**
	 * Subscriber f�r U_behind
	 */
	vm.U_behind.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behindstart);
			enableValidation(vm.U_behindstop);
		}
		else{
			disableValidation(vm.U_behindstart);
			disableValidation(vm.U_behindstop);
			disable(vm.U_behindpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behindpagar
	 */
	vm.U_behindpagar.subscribe(function (newValue) {
		if(vm.U_behind()){
			if (newValue){
				disableValidation(vm.U_behindstop);
			}
			else{
				enableValidation(vm.U_behindstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behaza
	 */
	vm.U_behaza.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behazastart);
			enableValidation(vm.U_behazastop);
		}
		else{
			disableValidation(vm.U_behazastart);
			disableValidation(vm.U_behazastop);
			disable(vm.U_behazapagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behazapaga
	 */
	vm.U_behazapagar.subscribe(function (newValue) {
		if(vm.U_behaza()){
			if (newValue){
				disableValidation(vm.U_behazastop);
			}
			else{
				enableValidation(vm.U_behazastop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behhyd
	 */
	vm.U_behhyd.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behhydstart);
			enableValidation(vm.U_behhydstop);
		}
		else{
			disableValidation(vm.U_behhydstart);
			disableValidation(vm.U_behhydstop);
			disable(vm.U_behhydpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behhydpagar
	 */
	vm.U_behhydpagar.subscribe(function (newValue) {
		if(vm.U_behhyd()){
			if (newValue){
				disableValidation(vm.U_behhydstop);
			}
			else{
				enableValidation(vm.U_behhydstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behery
	 */
	vm.U_behery.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_beherystart);
			enableValidation(vm.U_beherystop);
		}
		else{
			disableValidation(vm.U_beherystart);
			disableValidation(vm.U_beherystop);
			disable(vm.U_beherypagar);
		}
	});
	
	/**
	 * Subscriber f�r U_beherypagar
	 */
	vm.U_beherypagar.subscribe(function (newValue) {
		if(vm.U_behery()){
			if (newValue){
				disableValidation(vm.U_beherystop);
			}
			else{
				enableValidation(vm.U_beherystop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behimm
	 */
	vm.U_behimm.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behimmstart);
			enableValidation(vm.U_behimmstop);
		}
		else{
			disableValidation(vm.U_behimmstart);
			disableValidation(vm.U_behimmstop);
			disable(vm.U_behimmpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behimmpagar
	 */
	vm.U_behimmpagar.subscribe(function (newValue) {
		if(vm.U_behimm()){
			if (newValue){
				disableValidation(vm.U_behimmstop);
			}
			else{
				enableValidation(vm.U_behimmstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behlen
	 */
	vm.U_behlen.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behlenstart);
			enableValidation(vm.U_behlenstop);
		}
		else{
			disableValidation(vm.U_behlenstart);
			disableValidation(vm.U_behlenstop);
			disable(vm.U_behlenpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behlenpagar
	 */
	vm.U_behlenpagar.subscribe(function (newValue) {
		if(vm.U_behlen()){
			if (newValue){
				disableValidation(vm.U_behlenstop);
			}
			else{
				enableValidation(vm.U_behlenstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behkel
	 */
	vm.U_behkel.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behkelstart);
			enableValidation(vm.U_behkelstop);
		}
		else{
			disableValidation(vm.U_behkelstart);
			disableValidation(vm.U_behkelstop);
			disable(vm.U_behkelpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behkelpagar
	 */
	vm.U_behkelpagar.subscribe(function (newValue) {
		if(vm.U_behkel()){
			if (newValue){
				disableValidation(vm.U_behkelstop);
			}
			else{
				enableValidation(vm.U_behkelstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behend
	 */
	vm.U_behend.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behendstart);
			enableValidation(vm.U_behendstop);
		}
		else{
			disableValidation(vm.U_behendstart);
			disableValidation(vm.U_behendstop);
			disable(vm.U_behendpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behendpagar
	 */
	vm.U_behendpagar.subscribe(function (newValue) {
		if(vm.U_behend()){
			if (newValue){
				disableValidation(vm.U_behendstop);
			}
			else{
				enableValidation(vm.U_behendstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behann
	 */
	vm.U_behann.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_behanntxt);
			enableValidation(vm.U_behannstart);
			enableValidation(vm.U_behannstop);
		}
		else{
			disableValidation(vm.U_behanntxt);
			disableValidation(vm.U_behannstart);
			disableValidation(vm.U_behannstop);
			disable(vm.U_behannpagar);
		}
	});
	
	/**
	 * Subscriber f�r U_behannpagar
	 */
	vm.U_behannpagar.subscribe(function (newValue) {
		if(vm.U_behann()){
			if (newValue){
				disableValidation(vm.U_behannstop);
			}
			else{
				enableValidation(vm.U_behannstop);
			}
		}
	});
	
	/**
	 * Subscriber f�r U_behing
	 */
	vm.U_behing.subscribe(function (newValue) {
		if (newValue){
			disable(vm.U_behind);
			disable(vm.U_behaza);
			disable(vm.U_behhyd);
			disable(vm.U_behery);
			disable(vm.U_behimm);
			disable(vm.U_behlen);
			disable(vm.U_behkel);
			disable(vm.U_behend);
			disable(vm.U_behann);
		}
	});
	
	/**
	 * Subscriber f�r U_behenl
	 */
	vm.U_behenl.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '2'){
			disableValidation(vm.U_studprot);
		}
		else{
			enableValidation(vm.U_studprot);
		}
	});
	

	/* SUBSCRIBERS F�R "Har MDS-sjukdomen sedan f�reg�ende rapport transformerat till akut leukemi" */
	
	/**
	 * Subscriber f�r U_amltransform
	 */
	vm.U_amltransform.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_amltransformdat);
			disable(vm.U_amlbiobank);
			disable(vm.U_aspirat);
			disable(vm.U_biopsi);
		}
		else{
			enableValidation(vm.U_amltransformdat);
		}
	});
	
	/**
	 * Subscriber f�r U_aspirat
	 */
	vm.U_aspirat.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_aspiratlabklin);
			enableValidation(vm.U_aspiratprepnr);
			enableValidation(vm.U_aspiratprepar);
		}
		else{
			disableValidation(vm.U_aspiratlabklin);
			disableValidation(vm.U_aspiratprepnr);
			disableValidation(vm.U_aspiratprepar);
		}
	});
	
	/**
	 * Subscriber f�r U_biopsi
	 */
	vm.U_biopsi.subscribe(function (newValue) {
		if (newValue){
			enableValidation(vm.U_biopsilabklin);
			enableValidation(vm.U_biopsiprepnr);
			enableValidation(vm.U_biopsiprepar);
		}
		else{
			disableValidation(vm.U_biopsilabklin);
			disableValidation(vm.U_biopsiprepnr);
			disableValidation(vm.U_biopsiprepar);
		}
	});
	
	
	/* SUBSCRIBERS F�R "Biobanksprov vid transformation" */
	
	/**
	 * Subscriber f�r U_amlbiobank
	 */
	vm.U_amlbiobank.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '0'){
			disable(vm.U_amlbiobankdat);
		}
	});
	
	
	/* SUBSCRIBERS F�R "Allogen stamcellsplantation" */
	
	/**
	 * Subscriber f�r U_stamtransutf
	 */
	vm.U_stamtransutf.subscribe(function (newValue) {	
		if (newValue == undefined){
			disableValidation(vm.U_stamtransutfdat);
			disableValidation(vm.U_stamtransplan);
			disableValidation(vm.U_pathla);
		}
		else if(newValue.value == '0'){
			disableValidation(vm.U_stamtransutfdat);
			enableValidation(vm.U_stamtransplan);
			enableValidation(vm.U_pathla);
		}
		else{
			enableValidation(vm.U_stamtransutfdat);
			disableValidation(vm.U_stamtransplan);
			disableValidation(vm.U_pathla);
		}
	});
	

	/* SUBSCRIBERS F�R "D�dsfall" */
	
	/**
	 * Subscriber f�r U_patavliden
	 */
	vm.U_patavliden.subscribe(function (newValue) {	
		if (newValue == undefined || newValue.value != '1'){
			disableValidation(vm.U_patavlidendat);
			disableValidation(vm.U_patavlidenmds);
		}
		else{
			enableValidation(vm.U_patavlidendat);
			enableValidation(vm.U_patavlidenmds);
		}
	});
}


/**
 *  Funktion f�r att mutera registervariabler som kan p�verka validering.
 */
vm.mutateValues = function() {
	
	/* Patient �r ej uppf�ljd vid v�r klinik  */ 
	if(vm.U_ejuppfoljd())
		vm.U_ejuppfoljd.valueHasMutated();
	
	/* Blodprov */
	vm.U_blodprovdat.valueHasMutated();
	
	/* Prov till Nationella MDS biobanken */ 
	vm.U_natbioeftanm.valueHasMutated();
	
	/* Benm�rgsstatus */
	vm.U_benprovutf.valueHasMutated();
	if (vm.U_benprovutf() && vm.U_benprovutf().value == '1'){
		vm.U_cellhaltsak.valueHasMutated();
		vm.U_blastbenejbedom.valueHasMutated();
	}
	vm.U_blastben.valueHasMutated();
	vm.U_annbm.valueHasMutated();
	
	/* Har ny cytogenetik utf�rts sedan diagnos */
	vm.U_cytoutf.valueHasMutated();
	vm.U_karyotyp.valueHasMutated();
	vm.U_fishutf.valueHasMutated();
	
	/* Behandling */
	vm.U_behind.valueHasMutated();
	if(vm.U_behind()){
		vm.U_behindpagar.valueHasMutated();
	}
	vm.U_behaza.valueHasMutated();
	if(vm.U_behaza()){
		vm.U_behazapagar.valueHasMutated();
	}
	vm.U_behhyd.valueHasMutated();
	if(vm.U_behhyd()){
		vm.U_behhydpagar.valueHasMutated();
	}
	vm.U_behery.valueHasMutated();
	if(vm.U_behery()){
		vm.U_beherypagar.valueHasMutated();
	}
	vm.U_behimm.valueHasMutated();
	if(vm.U_behimm()){
		vm.U_behimmpagar.valueHasMutated();
	}
	vm.U_behlen.valueHasMutated();
	if(vm.U_behlen()){
		vm.U_behlenpagar.valueHasMutated();
	}
	vm.U_behkel.valueHasMutated();
	if(vm.U_behkel()){
		vm.U_behkelpagar.valueHasMutated();
	}
	vm.U_behend.valueHasMutated();
	if(vm.U_behend()){
		vm.U_behendpagar.valueHasMutated();
	}
	vm.U_behann.valueHasMutated();
	if(vm.U_behann()){
		vm.U_behannpagar.valueHasMutated();
	}
	
	vm.U_behenl.valueHasMutated();
	
	/* Har MDS-sjukdomen sedan f�reg�ende rapport transformerat till akut leukemi */
	vm.U_amltransform.valueHasMutated();
	if(vm.U_amltransform() && vm.U_amltransform().value == '1'){
		vm.U_aspirat.valueHasMutated();
		vm.U_biopsi.valueHasMutated();
	}
	
	/* Biobanksprov vid transformation */
	vm.U_amlbiobank.valueHasMutated();
	
	/* Allogen stamcellsplantation */
	vm.U_stamtransutf.valueHasMutated();
	
	/* D�dsfall */
	vm.U_patavliden.valueHasMutated();
}


/**
 *  Ber�knande variabel f�r att h�lla reda p� om minst en behandling �r ikryssad
 */
vm.behandlingChecked = ko.computed( function ( ) { 
	if(vm.U_behind() || vm.U_behaza() || vm.U_behhyd() || vm.U_behery() || vm.U_behimm() || 
		vm.U_behlen() || vm.U_behkel() || vm.U_behend() || vm.U_behann()){
		enableValidation(vm.U_behenl);
		return true;
	}	
	if(vm.U_behing()){
		disableValidation(vm.U_behenl);
		return true;
	}
	else{
		disableValidation(vm.U_behenl);
		return false;
	}	
});
	
	
/**
 *  Ber�knande variabel f�r att h�lla reda p� om blaster i blod �r �ver 20.0
 */
vm.highBlaster = ko.computed( function ( ) { 
	var value = vm.U_blastben();
	
	if(typeof value == 'string')
		value = value.replace(',','.');
	
	if(value && (parseFloat(value) > 20.0)){
		return true;
	}
	else{
		return false;
	}
});	

/**
 *  Ber�knande variabel f�r att h�lla reda p� att minst ett benm�rgsprov �r ikryssat
 */
vm.benmargsprovChecked = ko.computed( function ( ) { 
	if(vm.U_aspirat() || vm.U_biopsi()){
		return true;
	}	
	else{
		return false;
	}	
});	


/**
 *  Ber�knande variabel f�r att h�lla reda p� om resisterpost existerar
 */
vm.editingRegisterPost = ko.computed( function ( ) { 
	if(vm.reportingUser() &&
		vm.$vd.mds_uppfoljning_vd() && 
		vm.$vd.mds_uppfoljning_vd.listValues && 
		vm.getSelectedUppfoljning(vm.$vd.mds_uppfoljning_vd()).U_beddat != null
		){
		return true;
	}
	else{
		return false;
	}	
});
	
	
// k�r onload function
vm.runFunctionsOnLoad();


/**
 *  Applicera bindningar f�r vymodel
 */
ko.applyBindings( vm, inca.form.getContainer() );


/**
 *  k�r "afterload" funktion n�r formul�ret �r initializerat. (endast vid inrapportering)
 */	
if(vm.reportingUser()){	
	vm.init = ko.computed( function ( ) { 
		if(requestCount() == 0){
			vm.runFunctionsAfterLoad();
			vm.init.dispose();
		}	
	});
}
	
});